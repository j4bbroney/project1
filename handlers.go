package handlers

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func AddUser(c *gin.Context) {
	newUser := Users{}
	err := c.BindJSON(&newUser)
	if err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}
	c.BindJSON(&newUser)
	newUser.Date = time.Now()
	AllUsers = append(AllUsers, newUser)
}

func ShowUsers(c *gin.Context) {
	c.JSON(http.StatusOK, AllUsers)
}

func UserID(c *gin.Context) {
	f := c.Param("userID")
	switch f {
	case "1":
		c.JSON(http.StatusOK, AllUsers[0])
	case "2":
		c.JSON(http.StatusOK, AllUsers[1])
	case "3":
		c.JSON(http.StatusOK, AllUsers[2])
	default:
		c.JSON(http.StatusOK, "Пользователя с таким Id не существует")
	}
}
