package handlers

import "time"

type Users struct {
	Name string
	Mail string
	Id   string
	Date time.Time
}

var AllUsers = []Users{}
